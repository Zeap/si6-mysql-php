<?php

$connexion = mysqli_connect("172.16.99.3", "a.dominique", "passe", "a.dominique", 3306);
if(mysqli_connect_errno($connexion)){
	die('Connexion impossible : ' . mysqli_connect_error());
}

else{
$resultat = mysqli_query($connexion, "SELECT num, prenom, nom FROM Adherent;");

if($resultat != false){
	$nombre_tuples = mysqli_num_rows($resultat);
	print("La requête a retournée ".$nombre_tuples." tuples");
	print($nombre_tuples>1 ? "s":"");
	print("\n");

	$numero_tuple = 1;
	while($tuple = mysqli_fetch_assoc($resultat)){
		print("Le tuple numéro ". $numero_tuple ." identifiant " . $tuple['num'] . "  et se nomme " .$tuple['prenom'] ." ". $tuple['nom']. "\n");

		$numero_tuple++;
	}

	if(is_resource($resultat)){
		mysqli_free_result($resultat);
	}
}
mysqli_close($connexion);
}
?>
